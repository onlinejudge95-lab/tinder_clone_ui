import axios from 'axios';


const instance = axios.create({
    baseURL: "https://tinder-clone-backend-example.herokuapp.com"
});

export default instance;
